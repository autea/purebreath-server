FROM python:3-onbuild

EXPOSE 5000
ENV DEBUG=0

CMD ["python", "./app.py"]
