# -*- coding: utf-8 -*-
import os
from datetime import datetime

from flask import Flask, g, request, abort
from flask_socketio import SocketIO, emit
from flask_restful import Resource, Api
import rethinkdb as r
from rethinkdb.errors import RqlRuntimeError, ReqlOpFailedError, RqlDriverError

DEBUG = os.environ.get('DEBUG', True)
DEBUG = False if DEBUG != '1' else True
RDB_HOST = os.environ.get('RDB_HOST', 'localhost')
RDB_PORT = os.environ.get('RDB_PORT', '28015')

app = Flask(__name__)
app.config['SECRET_KEY'] = 'myveryfunnysecret.'
api = Api(app)
socketio = SocketIO(app)

conn = r.connect(RDB_HOST, RDB_PORT)
DB = 'purebreath'

try:
    r.db_create(DB).run(conn)
    r.db(DB).table_create('pollutions').run(conn)
    r.db(DB).table('pollutions').index_create('geo', geo=True).run(conn)
except ReqlOpFailedError:
    print('DB already exists')
finally:
    conn.close()


@app.before_request
def before_request():
    try:
        g.rdb_conn = r.connect(host=RDB_HOST, port=RDB_PORT, db=DB)
    except RqlDriverError:
        abort(503, "No database connection could be established.")


@app.teardown_request
def teardown_request(exception):
    try:
        g.rdb_conn.close()
    except AttributeError:
        pass


class PollutionsInput(Resource):
    def post(self):
        print(request.json)

        try:
            val = request.json['value']
            lat = request.json['geo']['latitude']
            long = request.json['geo']['longitude']
        except KeyError:
            abort(400)

        try:
            data = {
                'value': val,
                'geo': {
                    'latitude': lat,
                    'longitude': long
                },
                'timestamp': datetime.now(r.make_timezone('00:00'))
            }
            r.table('pollutions').insert(data).run(g.rdb_conn)
        except RqlRuntimeError:
            print('ERROR: cannot insert data: {}'.format(data))
            abort(500)

        emit('newValue', data, namespace='pollutions', broadcast=True)

        return 200


@socketio.on('connection')
def send_changes():
    print('New connection')
    # emit('newValue', )



api.add_resource(PollutionsInput, '/v1/pollutions')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', debug=DEBUG)
